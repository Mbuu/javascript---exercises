function FormatError(message) {
    this.name = "FormatError";
    this.message = message;

    if (Error.captureStackTrace) {
        Error.captureStackTrace(this, this.constructor);
    } else {
        this.stack = (new Error()).stack;
    }

}

FormatError.prototype = Object.create(SyntaxError.prototype);
FormatError.prototype.constructor = FormatError;

var err = new FormatError("ошибка форматирования");

alert( err.message );
alert( err.name );
alert( err.stack );
alert( err instanceof SyntaxError );