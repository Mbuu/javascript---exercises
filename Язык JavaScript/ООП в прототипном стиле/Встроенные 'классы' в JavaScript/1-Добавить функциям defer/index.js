Function.prototype.defer = function (delay) {
    setTimeout(this, delay)
};

function f() {
    alert( "привет" );
}

f.defer(2000); // выведет "привет" через 2 секунды