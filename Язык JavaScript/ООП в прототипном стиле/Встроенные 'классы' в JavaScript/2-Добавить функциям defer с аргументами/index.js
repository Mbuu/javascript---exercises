Function.prototype.defer = function (delay) {
    let f = this;
    return function () {
        let context = this;
        let args = arguments;
        setTimeout(function () {
            f.apply(context, args)
        }, delay)
    }
};


function f(a, b) {
    alert( a + b );
}

f.defer(1000)(1, 2); // выведет 3 через 1 секунду.