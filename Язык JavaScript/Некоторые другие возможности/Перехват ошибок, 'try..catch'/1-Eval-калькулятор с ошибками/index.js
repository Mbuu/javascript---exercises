let exp, res;

while (true) {
    exp = prompt('Insert expression:', '2-');
    if (exp == null) break;

    try {
        res = eval(exp);
        if (isNaN(res)) {
            throw new Error('result is not defined');
        }

        break;
    } catch (e) {
        alert(e.message + ", repeat input");
    }
}

alert(res);