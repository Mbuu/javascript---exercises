/*function findInArray(arr, value) {
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] === value) {
            return i;
        }
    }
    return -1;
}*/

function findInArray(arr, value) {
    return arr.indexOf(value);
}

let arr = ["test", 2, 1.5, false];

console.log(findInArray(arr, 2));