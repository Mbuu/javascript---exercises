let array = [];

for (var i = 2; i < 100; i++) {
    array[i] = i;
}

let p = 2;

do {
    for (i = 2 * p; i < 100; i += p) {
        array[i] = false;
    }

    for (i = p + 1; i < 100; i++) {
        if (array[i]) break;
    }

    p = i;
} while(p * p < 100);

let newArray = array.filter(function (item) {
    return !!item;
});

console.dir(newArray);