let styles = ['Jazz', 'Blues'];
console.log(styles);

styles.push('Rock');
console.log(styles);

styles[styles.length - 2] = 'Classic';
console.log(styles);

alert(styles.shift());
console.log(styles);

styles.unshift('Rap', 'Reggae');
console.log(styles);

