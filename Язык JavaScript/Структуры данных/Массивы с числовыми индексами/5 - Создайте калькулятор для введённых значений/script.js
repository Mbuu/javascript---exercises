let arr = [];
let sum = 0;

while (true) {
    let inputValue = prompt('insert value: ', '0');
    if (inputValue === '' || inputValue === null || !isNumeric(inputValue)) break;

    arr.push(+inputValue);
    sum += +inputValue;
}

console.log(arr);
console.log(sum);

function isNumeric(num) {
    return !isNaN(parseFloat(num)) && isFinite(num)
}