/*function filterRange(arr, a, b) {
    let newArr = [];

    for (let i = 0; i < arr.length; i++) {
        if (arr[i] >= a && arr[i] <= b) {
            newArr.push(arr[i])
        }
    }
    return newArr;
}*/

function filterRange(arr, a, b) {
    let newArray = arr.filter(function (item) {
        return (item >= a && item <= b)
    });

    return newArray.sort(function (a, b) {
        return a - b;
    });
}


console.log(filterRange([5, 4, 3, 8, 0], 3, 5));