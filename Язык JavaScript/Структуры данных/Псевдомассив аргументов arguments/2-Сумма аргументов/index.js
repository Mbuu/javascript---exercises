function sumArg() {
    let sum = 0;

    for (let i = 0; i < arguments.length; i++) {
        sum += arguments[i]
    }

    return sum;
}

console.log(sumArg());
console.log(sumArg(1));
console.log(sumArg(1, 2));
console.log(sumArg(1, 2, 3));
console.log(sumArg(1, 2, 3, 4));