let strings = ["кришна", "кришна", "харе", "харе",
    "харе", "харе", "кришна", "кришна", "8-()"
];

function unique(arr) {

    let obj = {};

    for (let i = 0; i < arr.length; i++) {
        obj[arr[i]] = true;
    }

    return Object.keys(obj);
}

console.log(unique(strings));
