let object = {
    className: 'open menu'
};

function addClass(obj, cls) {

    if (obj.className) {
        let classNamesArray = obj.className.split(' ');
        if (classNamesArray.indexOf(cls) === -1) {
            classNamesArray.push(cls);
            obj.className = classNamesArray.join(' ');
        }
    } else {
        return 'object doesn\'t have property "className"';
    }
    return obj;
}

console.log(addClass(object, 'new'));
console.log(addClass(object, 'open'));
console.log(addClass(object, 'me'));

