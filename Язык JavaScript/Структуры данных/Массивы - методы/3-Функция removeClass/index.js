let obj = {
    className: 'my menu menu'
};

function removeClass(obj, cls) {

    if (obj.className) {
        let classNamesArray = obj.className.split(' ');
        for (let i = 0; i < classNamesArray.length; i++) {
            if (classNamesArray[i] === cls) {
                classNamesArray.splice(i, 1);
                i--;
            }
        }
        obj.className = classNamesArray.join(' ');
    } else {
        return 'object doesn\'t have property "className"';
    }
    return obj;
}

removeClass(obj, 'menu');

console.log(obj);