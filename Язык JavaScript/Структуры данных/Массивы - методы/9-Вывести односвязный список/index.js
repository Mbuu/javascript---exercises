let list = {
    value: 1,
    next: {
        value: 2,
        next: {
            value: 3,
            next: {
                value: 4,
                next: null
            }
        }
    }
};

function printList(list) {
    let temp = list;

    while (temp) {
        console.log(temp.value);
        temp = temp.next;
    }
}

printList(list);
console.log('\n');

function printListRecursion(list) {
    console.log(list.value);

    if (list.next) {
        printListRecursion(list.next)
    }
}

printListRecursion(list);
console.log('\n');

function printReverseListRecursion(list) {
    
    if (list.next) {
        printReverseListRecursion(list.next);
    }

    console.log(list.value)
}

printReverseListRecursion(list);
console.log('\n');

function printReverseList(list) {
    let temp = list;
    let arr = [];
    
    while (temp) {
        arr.push(temp.value);
        temp = temp.next;
    }

    for (let i = arr.length - 1; i >= 0; i--) {
        console.log(arr[i]);
    }
}

printReverseList(list);