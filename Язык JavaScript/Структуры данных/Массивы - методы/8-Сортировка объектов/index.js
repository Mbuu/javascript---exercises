let vasya = { name: "Вася", age: 23 };
let masha = { name: "Маша", age: 18 };
let vovochka = { name: "Вовочка", age: 6 };

let people = [ vasya , masha , vovochka ];

function compareObjectsByAge(obj1, obj2) {
    return obj1.age - obj2.age;
}

people.sort(compareObjectsByAge);

for(let i = 0; i < people.length; i++) {
    console.log(people[i].name);
}