let arr = ["воз", "киборг", "корсет", "ЗОВ", "гробик", "костер", "сектор"];
console.log(arr);

function aClean(arr) {

    let obj = {};

    for (let i = 0; i < arr.length; i++) {
        let sorted = arr[i].toLowerCase().split('').sort().join('');
        obj[sorted] = arr[i];
    }

    let newArr = [];

    for (let key in obj) {
        newArr.push(obj[key])
    }

    return newArr;
    }

console.log(aClean(arr));