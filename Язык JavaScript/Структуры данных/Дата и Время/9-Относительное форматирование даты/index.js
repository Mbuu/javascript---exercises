function formatDate(date) {

    let diff = new Date() - date;

    if (diff < 1000) {
        return "только что";
    }

    let sec = Math.floor(diff / 1000);

    if (sec < 60) {
        return sec + " сек. назад";
    }

    let min = Math.floor(diff / 60000);

    if (min < 60) {
        return min + " мин. назад";
    }

    let formatter = new Intl.DateTimeFormat("ru", {
        day: '2-digit',
        month: '2-digit',
        year: '2-digit',
        hour: '2-digit',
        minute: '2-digit'
    });

    return formatter.format(date);
}

alert(formatDate(new Date(new Date - 1))); // "только что"
alert(formatDate(new Date(new Date - 30 * 1000))); // "30 сек. назад"
alert(formatDate(new Date(new Date - 5 * 60 * 1000))); // "5 мин. назад"
alert(formatDate(new Date(new Date - 86400 * 1000))); // вчерашняя дата в формате "дд.мм.гг чч:мм"
