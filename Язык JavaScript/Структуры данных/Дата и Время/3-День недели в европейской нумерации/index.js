function getLocalDay(date) {

    let arrWeek = [7, 1, 2, 3, 4, 5, 6];
    return arrWeek[date.getDay()];

}

let date = new Date(2012, 0, 3);  // 3 янв 2012
alert( getLocalDay(date) );       // вторник, выведет 2