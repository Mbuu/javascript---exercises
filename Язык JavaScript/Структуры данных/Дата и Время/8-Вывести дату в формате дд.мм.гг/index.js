function formatDate(date) {
    let day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
    let month = (date.getMonth() + 1) < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1;
    let year = (date.getFullYear() % 100) < 10 ? '0' + (date.getFullYear() % 100) : date.getFullYear() % 100;

    return day + '.' + month +'.' + year;
}

let d = new Date(2007, 0, 30); // 30 января 2014
alert( formatDate(d) ); // '30.01.14'