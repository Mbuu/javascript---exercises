function getSecondsToTomorrow() {
    let now = new Date();
    return 24 * 60 * 60 - (now.getHours() * 3600 + now.getMinutes() * 60 + now.getSeconds());
}

alert( getSecondsToTomorrow() );