let menu = {
    width: 200,
    height: 300,
    title: "400"
};

multiplyNumeric(menu);

console.log(menu)

function isNumeric(num) {
    return !isNaN(parseFloat(num) && isFinite(num));
}

function multiplyNumeric(obj) {

    for (let key in obj) {
        if (isNumeric(obj[key])) {
            obj[key] *= 2;
        }
    }

}