'use strict';

let urls = ['user.json', 'guest.json'];

let chain = Promise.resolve();

let results = [];

urls.forEach(function(url) {
  chain = chain
    .then(() => httpGet(url))
    .then((result) => {
      results.push(result);
    });
});

chain.then(() => {
  alert(results);
});