function Machine(power) {
    this._power = power;
    this._enabled = false;

    let self = this;

    this.enable = function () {
        self._enabled = true;
    };

    this.disable = function () {
        self._enabled = false;
    };
}

function Fridge(power) {
    Machine.apply(this, arguments);

    let food = [];

    this.addFood = function () {
        if (!this._enabled) {
            throw new Error("Fridge is off")
        }
        if (food.length + arguments.length > this._power / 100) {
            throw new Error("Fridge is full")
        }
        for (let i = 0; i < arguments.length; i++) {
            food.push(arguments[i])
        }
    };

    this.getFood = function () {
        return food.slice();
    };

    this.removeFood = function () {
        for (let i = 0; i < arguments.length; i++) {
            if (~food.lastIndexOf(arguments[i])) {
                food.splice(food.indexOf(arguments[i]), 1)
            }
        }
    };
    
    this.filterFood = function (filter) {
        return food.filter(filter);
    }
}

var fridge = new Fridge(500);
fridge.enable();
fridge.addFood({
    title: "котлета",
    calories: 100
});
fridge.addFood({
    title: "сок",
    calories: 30
});
fridge.addFood({
    title: "зелень",
    calories: 10
});
fridge.addFood({
    title: "варенье",
    calories: 150
});

var dietItems = fridge.filterFood(function(item) {
    return item.calories < 50;
});

fridge.removeFood("нет такой еды"); // без эффекта
alert( fridge.getFood().length ); // 4

dietItems.forEach(function(item) {
    alert( item.title ); // сок, зелень
    fridge.removeFood(item);
});

alert( fridge.getFood().length ); // 2