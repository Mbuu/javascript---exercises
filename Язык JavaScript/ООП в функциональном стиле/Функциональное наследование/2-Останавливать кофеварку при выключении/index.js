function Machine(power) {
    this._enabled = false;

    this.enable = function () {
        this._enabled = true;
    };

    this.disable = function () {
        this._enabled = false;
    };
}

function CoffeeMachine(power, capacity) {
    Machine.apply(this, arguments);

    let waterAmount = 0;
    let WATER_HEAT_CAPACITY = 4200;

    let parentDisable = this.enable;

    this.disable = function () {
        parentDisable.call(this);
        clearTimeout(timerId);
    };

    function getTimeToBoil() {
        return waterAmount * WATER_HEAT_CAPACITY * 80 / power;
    }

    this.setWaterAmount = function (amount) {
        if (amount < 0) {
            throw new Error("Значение должно быть положительным");
        }
        if (amount > capacity) {
            throw new Error("Нельзя залить больше, чем " + capacity);
        }

        waterAmount = amount;
    };

    this.getWaterAmount = function () {
        return waterAmount;
    };

    this.addWater = function (amount) {
        this.setWaterAmount(waterAmount + amount);
    };

    this.getPower = function () {
        return power;
    };

    function onReady() {
        alert('Кофе готов!');
    }

    this.setOnReady = function (newOnReady) {
        onReady = newOnReady;
    };

    let timerId;

    this.run = function () {
        if (this._enabled) {
            timerId = setTimeout(function () {
                    timerId = null;
                    onReady()
                }, getTimeToBoil()
            );
        } else {
            throw new Error('Кофеварка выключена')
        }

    };

    this.isRunning = function () {
        return !!timerId;
    }

}

var coffeeMachine = new CoffeeMachine(10000);
coffeeMachine.enable();
coffeeMachine.run();
coffeeMachine.disable(); // остановит работу, ничего не выведет