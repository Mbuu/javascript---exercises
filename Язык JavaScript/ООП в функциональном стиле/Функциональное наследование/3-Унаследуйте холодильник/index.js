function Machine(power) {
    this._power = power;
    this._enabled = false;

    let self = this;

    this.enable = function () {
        self._enabled = true;
    };

    this.disable = function () {
        self._enabled = false;
    };
}

function Fridge(power) {
    Machine.apply(this, arguments);

    let food = [];

    this.addFood = function () {
        if (!this._enabled) {
            throw new Error("Fridge is off")
        }
        if (food.length + arguments.length > this._power / 100) {
            throw new Error("Fridge is full")
        }
        for (let i = 0; i < arguments.length; i++) {
            food.push(arguments[i])
        }
    };

    this.getFood = function () {
        return food.slice();
    }
}

var fridge = new Fridge(500);
fridge.enable();
fridge.addFood("котлета");
fridge.addFood("сок", "варенье");

var fridgeFood = fridge.getFood();
alert(fridgeFood); // котлета, сок, варенье

// добавление элементов не влияет на еду в холодильнике
fridgeFood.push("вилка", "ложка");

alert(fridge.getFood()); // внутри по-прежнему: котлета, сок, варенье