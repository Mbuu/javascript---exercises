function User() {
    let name = 'Vasia';
    let surname = 'Dodik';

    this.setName = function (n) {
        name = n;
    };

    this.setSurname = function (sn) {
        surname = sn;
    };

    this.getFullName = function () {
        return name + ' ' + surname;
    }
}

let user = new User();
user.setName("Петя");
user.setSurname("Иванов");

alert( user.getFullName() );