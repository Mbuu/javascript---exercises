function CoffeeMachine(power, capacity) {
    let waterAmount = 0;

    this.getPower = function () {
        return power;
    };

    this.setWaterAmount = function(amount) {
        if (amount < 0) {
            throw new Error("Значение должно быть положительным");
        }
        if (amount > capacity) {
            throw new Error("Нельзя залить воды больше, чем " + capacity);
        }

        waterAmount = amount;
    };

    this.getWaterAmount = function() {
        return waterAmount;
    };

}

let coffeMachine = new CoffeeMachine(1000, 2000);
console.log(coffeMachine.getPower());