function Article() {
    this.created = new Date();
    Article.count++;
    Article.last = this.created;
    let self = this;

    Article.formatDate = function (date) {
        let day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
        let mth = (date.getMonth() + 1) < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1;
        let year = date.getFullYear();
        let hrs = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
        let min = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
        let sec = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
        let mls = date.getMilliseconds();
      return `${day}.${mth}.${year}, ${hrs}:${min}:${sec}:${mls}`
    };
}

Article.showStats = function () {
    console.log(`Всего: ${this.count}, Последняя: ${Article.formatDate(this.last)}`)
};

Article.count = 0;

new Article();
new Article();
new Article();

Article.showStats(); // Всего: 2, Последняя: (дата)

new Article();

Article.showStats(); // Всего: 3, Последняя: (дата)