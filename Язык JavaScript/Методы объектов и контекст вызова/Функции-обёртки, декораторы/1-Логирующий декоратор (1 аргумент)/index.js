function makeLogging(f, log) {
    return function(a) {
        log.push(a);
        return f.call(this, a);
    }
}

function work(a) {
    return a / 2;
}

let log = [];
work = makeLogging(work, log);

work(1); // 1, добавлено в log
work(5); // 5, добавлено в log

for (let i = 0; i < log.length; i++) {
    alert( 'Лог:' + log[i] ); // "Лог:1", затем "Лог:5"
}