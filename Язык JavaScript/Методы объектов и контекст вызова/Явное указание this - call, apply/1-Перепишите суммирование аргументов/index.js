function sumArgs() {
    return Array.isArray(arguments[0]) ?
        arguments[0].reduce(function(a, b) {
        return a + b;
        }) :
        [].reduce.call(arguments, function(a, b) {
            return a + b;
        })
}

console.log( sumArgs(1, 2, 3) );