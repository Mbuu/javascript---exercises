function makeBuffer() {
    let store = '';

    function buffer(value) {
        if (arguments.length === 0) {
            return store;
        } else {
            store += value;
        }
    }

    buffer.clear = function () {
        store = '';
    };

    return buffer;
}

var buffer = makeBuffer();

buffer("Тест");
buffer(" тебя не съест ");
alert( buffer() ); // Тест тебя не съест

buffer.clear();

alert( buffer() ); // ""