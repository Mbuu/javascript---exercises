function sumByClosure(a) {

    return function (b) {
        return a + b;
    }
}

console.log(sumByClosure(1)(2));
console.log(sumByClosure(5)(-1));