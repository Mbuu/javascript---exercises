Object.defineProperty(window, 'pageYOffset', {
    get: function () {
        return document.documentElement.scrollTop;
    }
});

console.log(window.pageYOffset);