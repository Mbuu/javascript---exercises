function showNotification(options) {
    let notification = document.createElement('div');
    notification.className = 'notification';

    if (options.className) {
        notification.classList.add(options.className);
    }

    if (options.cssText) {
        notification.style.cssText = options.cssText;
    }

    notification.style.top = (options.top || 0) + 'px';
    notification.style.right = (options.top || 0) + 'px';

    notification.innerHTML = options.html || 'Hello';

    document.body.appendChild(notification);

    setTimeout(function () {
        document.body.removeChild(notification)
    }, 1500);
}

let i = 0;

setInterval(function () {
    showNotification({
        className: 'welcome',
        html: 'Hello ' + ++i,
        top: 10,
        right: 10
    });
}, 2000);
