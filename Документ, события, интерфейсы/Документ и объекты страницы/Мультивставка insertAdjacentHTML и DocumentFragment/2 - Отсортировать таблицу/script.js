function createTable() {
    let tableHolder = document.getElementById('table-holder');
    let contents = '<thead><th>Name</th><th>Surname</th><th>Patronymic</th><th>Age</th></thead>';
    contents += '<tbody>';

    for (let i = 0; i < 100; i++) {
        contents += '<tr><td>...</td><td>...</td><td>...</td><td>' + Math.round(Math.random() * 100) + '</td></tr>'
    }

    contents += '</tbody>';

    tableHolder.innerHTML = '<table>' + contents + '</table>';
}

createTable();

function sortTableByAge() {
    let tbody = document.getElementsByTagName('tbody')[0];
    let rows = [];

    for (let i = 0; i < tbody.children.length; i++) {
        rows.push(tbody.children[i]);
    }

    rows.sort(function (a, b) {
        return a.lastChild.innerHTML - b.lastChild.innerHTML;
    });

    for (let i = 0; i < rows.length; i++) {
        tbody.appendChild(rows[i]);
    }

}

sortTableByAge();