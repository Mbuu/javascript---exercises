let fieldCoordinates = field.getBoundingClientRect();

console.log('1 - ' + Math.round(fieldCoordinates.left) + ':' + Math.round(fieldCoordinates.top));
console.log('2 - ' + Math.round(fieldCoordinates.right) + ':' + Math.round(fieldCoordinates.bottom));
console.log('3 - ' + Math.round(fieldCoordinates.left + field.clientLeft) + ':'
                   + Math.round(fieldCoordinates.top + field.clientTop));
console.log('4 - ' + Math.round(fieldCoordinates.left + field.clientLeft + field.clientWidth) + ':'
                   + Math.round(fieldCoordinates.top + field.clientTop + field.clientHeight));