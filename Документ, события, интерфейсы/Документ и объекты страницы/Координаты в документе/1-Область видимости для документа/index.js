function getDocumentScroll() {
    let obj = {};
    obj.top = Math.floor(window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop);
    obj.bottom = obj.top + window.innerHeight;
    obj.height = Math.max(
        document.body.scrollHeight, document.documentElement.scrollHeight,
        document.body.offsetHeight, document.documentElement.offsetHeight,
        document.body.clientHeight, document.documentElement.clientHeight
    );

    return obj;
}

console.log(getDocumentScroll());