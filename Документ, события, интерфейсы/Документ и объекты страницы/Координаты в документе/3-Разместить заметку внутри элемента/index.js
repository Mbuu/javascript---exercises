function getCoords(elem) {
    let box = elem.getBoundingClientRect();

    let body = document.body;
    let html = document.documentElement;

    let scrollTop = window.pageYOffset || body.scrollTop || html.scrollTop;
    let scrollLeft = window.pageXOffset || body.scrollLeft || html.scrollLeft;
    console.log( scrollTop );
    console.log( scrollLeft );

    let clientTop = html.clientTop || body.clientTop || 0;
    let clientLeft = html.clientLeft || body.clientLeft || 0;
    console.log(clientTop);
    console.log(clientLeft);

    let top = box.top + scrollTop - clientTop;
    let left = box.left + scrollLeft - clientLeft;
    console.log(top);
    console.log(left);

    return {
        top: top,
        left: left
    };
}
function positionAt(anchor, position, elem) {
    let anchorCoords = getCoords(anchor);

    switch (position) {
        case 'top-out':
            elem.style.left = anchorCoords.left + 'px';
            elem.style.top = anchorCoords.top - elem.offsetHeight + 'px';
            break;

        case 'right-out':
            elem.style.left = anchorCoords.left + anchor.offsetWidth + 'px';
            elem.style.top = anchorCoords.top + 'px';
            break;

        case 'bottom-out':
            elem.style.left = anchorCoords.left + 'px';
            elem.style.top = anchorCoords.top + anchor.offsetHeight + 'px';
            break;

            case 'top-in':
            elem.style.left = anchorCoords.left + 'px';
            elem.style.top = anchorCoords.top + 'px';
            break;

        case 'right-in':
            elem.style.left = anchorCoords.left + anchor.offsetWidth - elem.offsetWidth + 'px';
            elem.style.top = anchorCoords.top + 'px';
            break;

        case 'bottom-in':
            elem.style.left = anchorCoords.left + 'px';
            elem.style.top = anchorCoords.top + anchor.offsetHeight - elem.offsetHeight + 'px';
            break;
    }
}

/**
 * Показывает заметку с текстом html на позиции position
 * относительно элемента anchor
 */
function showNote(anchor, position, html) {
    let note = document.createElement('div');
    note.className = 'note';
    note.innerHTML = html;
    document.body.appendChild(note);

    positionAt(anchor, position, note);
}

// проверка работоспособности
let blockquote = document.querySelector('blockquote');

showNote(blockquote, "top-in", "заметка сверху");
showNote(blockquote, "right-in", "заметка справа");
showNote(blockquote, "bottom-in", "заметка снизу");