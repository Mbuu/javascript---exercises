let div = document.getElementById('moving-div');
let divHeight = div.offsetHeight + 'px';
let newDiv = document.createElement('div');
newDiv.style.height = divHeight;
newDiv.style.marginTop = getComputedStyle(div).marginTop;
newDiv.style.marginBottom = getComputedStyle(div).marginBottom;
newDiv.style.background = 'black';
document.body.insertBefore(newDiv, div);
div.style.position = 'absolute';
div.style.right = div.style.top = 0;

