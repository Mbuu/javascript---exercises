let slider = document.getElementById('slider');
let thumb = document.querySelector('.thumb');

function getCoords(elem) {
    let box = elem.getBoundingClientRect();

    return {
        top: box.top + pageYOffset,
        left: box.left + pageXOffset
    }
}

thumb.onmousedown = function (e) {
    thumb.style.cursor = 'grabbing';
    let thumbCoords = getCoords(thumb);
    let shiftX = e.pageX - thumbCoords.left;

    let sliderCoords = getCoords(slider);

    document.onmousemove = function (e) {
        document.body.style.cursor = 'grabbing';
        let newLeft = e.pageX - shiftX - sliderCoords.left;

        if (newLeft < 0) {
            newLeft = 0;
        }

        let rightEdge = slider.offsetWidth - thumb.offsetWidth;

        if (newLeft > rightEdge) {
            newLeft = rightEdge;
        }

        thumb.style.left = newLeft + 'px';
    };

    document.onmouseup = function () {
        thumb.style.cursor = 'grab';
        document.body.style.cursor = 'default';
        document.onmousemove = document.onmouseup = null;
    };

    return false;
};

thumb.ondragstart = function () {
    return false;
};
