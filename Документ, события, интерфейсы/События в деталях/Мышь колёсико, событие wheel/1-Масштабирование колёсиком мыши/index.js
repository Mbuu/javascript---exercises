let scale = 1;
text.onwheel = function (e) {
    let delta = e.deltaY;

    if (delta < 0) {
        scale += 0.05;
    } else {
        scale -= 0.05
    }

    text.style.transform = `scale(${scale})`;

    return false;
};