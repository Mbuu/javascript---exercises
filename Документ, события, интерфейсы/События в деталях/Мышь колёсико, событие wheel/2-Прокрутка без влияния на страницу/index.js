document.onwheel = function (e) {

    if (e.target.tagName !== 'TEXTAREA') return;
    let area = e.target;
    let delta = e.deltaY;
    
    if (delta < 0 && area.scrollTop === 0) {
        e.preventDefault();
    }
    
    if (delta > 0 && area.scrollHeight - area.getBoundingClientRect().height - area.scrollTop <= 0) {
        e.preventDefault()
    }

};
