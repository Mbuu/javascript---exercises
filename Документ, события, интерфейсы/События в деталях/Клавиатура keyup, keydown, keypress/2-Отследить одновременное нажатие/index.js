function runOnKeys(func) {
    let codes = [].slice.call(arguments, 1);
    let pressed = {};

    document.onkeydown = function (e) {
        pressed[e.keyCode] = true;

        for (let i = 0; i < codes.length; i++) {
            if(!pressed[codes[i]]) return;
        }

        pressed = {};

        func();
    };

    document.onkeyup = function (e) {
        delete pressed[e.keyCode]
    }
}



runOnKeys(
    function () {
        alert("Привет!")
    },
    "Q".charCodeAt(0),
    "W".charCodeAt(0)
);
