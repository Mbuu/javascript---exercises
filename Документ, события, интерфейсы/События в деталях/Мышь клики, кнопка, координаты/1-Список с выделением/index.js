let selected = document.querySelector('ul');
let listItem = document.getElementsByTagName('li');

selected.onclick = function (e) {

    if (e.target.tagName !== 'LI') return;

    for (let i = 0; i < listItem.length; i++) {
        if (!e.ctrlKey && !e.shiftKey) {
            listItem[i].classList.remove('selected');
        }
    }
    if ((e.ctrlKey || e.metaKey) && e.target.tagName === 'LI') {
        e.target.classList.toggle('selected');
    } else if (e.target.nodeName === 'LI') {
        e.target.classList.add('selected');
    }

    if (e.shiftKey) {
        for (let i = Array.prototype.indexOf.call(listItem, document.querySelector('.selected')); i < Array.prototype.indexOf.call(listItem, e.target); i++) {
            listItem[i].classList.add('selected');
        }
    }

};