let animalList = document.querySelector('.animal-tree__list');

animalList.onclick = function (event) {
    if (event.target.tagName !== 'LI') return;

    if (event.target.children[0]) {
        event.target.children[0].classList.toggle('collapse')
    }

};