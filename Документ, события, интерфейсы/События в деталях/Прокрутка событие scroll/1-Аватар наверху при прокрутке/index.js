let avatar = document.getElementById('avatar');
let avatarBottom = avatar.getBoundingClientRect().bottom + window.pageYOffset;

window.onscroll = function () {
    if (window.pageYOffset < avatarBottom && avatar.classList.contains('fixed')) {
        avatar.classList.remove('fixed')
    } else if (window.pageYOffset > avatarBottom) {
        avatar.classList.add('fixed')
    }
};