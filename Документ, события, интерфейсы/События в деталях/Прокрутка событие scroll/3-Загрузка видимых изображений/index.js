function isVisible(elem) {

    let coords = elem.getBoundingClientRect();
    let windowHeight = document.documentElement.clientHeight;

    // верхняя граница elem в пределах видимости ИЛИ нижняя граница видима
    let topVisible = coords.top > 0 && coords.top < windowHeight;
    let bottomVisible = coords.bottom < windowHeight && coords.bottom > 0;

    return topVisible || bottomVisible;
}

function showVisible() {
    let images = document.getElementsByTagName('img');

    for (let i = 0; i < images.length; i++) {
        let img = images[i];
        let realsrc = img.getAttribute('realsrc');
        if (!realsrc) continue;

        if (isVisible(img)) {
            img.src = realsrc;
            img.setAttribute('realsrc', '');
        }
    }
}

window.onscroll = showVisible;
showVisible();