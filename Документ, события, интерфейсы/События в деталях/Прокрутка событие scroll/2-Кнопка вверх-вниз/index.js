let arrow = document.getElementById('arrow');
let pageYLabel = 0;

arrow.onclick = function () {
    let pageY = window.pageYOffset || document.documentElement.scrollTop;

    switch (this.className) {
        case 'up':
            pageYLabel = pageY;
            window.scrollTo(0, 0);
            this.className = 'down';
            break;

        case 'down':
            window.scrollTo(0, pageYLabel);
            this.className = 'up';
    }
};

window.onscroll = function () {
    let pageY = window.pageYOffset || document.documentElement.scrollTop;
    let innerHeight = window.innerHeight;

    switch (arrow.className) {
        case '':
            if (pageY > innerHeight) {
                arrow.className = 'up';
            }
            break;

        case 'up':
            if (pageY < innerHeight) {
                arrow.className = '';
            }
            break;

        case 'down':
            if (pageY > innerHeight) {
                arrow.className = 'up';
            }
            break;
    }
};
