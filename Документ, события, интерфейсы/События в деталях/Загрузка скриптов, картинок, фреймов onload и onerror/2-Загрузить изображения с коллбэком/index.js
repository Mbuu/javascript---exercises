function preloadImages(sources, callback) {
    let counter = 0;

    for (let i = 0; i < sources.length; i++) {
        let img = document.createElement('img');
        img.onload = img.onerror = function () {
            counter++;
            
            if (counter === sources.length) {
                callback();
            }
        };
        img.src = sources[i];
        setTimeout(function () {
            document.body.appendChild(img)
        }, 1000);
    }
}

// ---------- Проверка ----------

/* файлы для загрузки */
let sources = [
    "https://js.cx/images-load/1.jpg",
    "https://js.cx/images-load/2.jpg",
    "https://js.cx/images-load/3.jpg"
];
for (let i = 0; i < sources.length; i++) {
    sources[i] += '?' + Math.random(); // добавляем параметр, чтобы без кеша (для теста)
}

/** если картинка загружена, то можно будет сразу получить её ширину
 * создадим все картинки и проверим, есть ли у них ширина
 */
function testLoaded() {
    let widthSum = 0;
    for (let i = 0; i < sources.length; i++) {
        let img = document.createElement('img');
        img.src = sources[i];
        widthSum += img.width;
    }
    // каждое изображение 100x100, общая ширина должна быть 300px
    alert(widthSum);
}

// до загрузки - выведет 0
testLoaded();

// после загрузки - выведет 300
preloadImages(sources, testLoaded);