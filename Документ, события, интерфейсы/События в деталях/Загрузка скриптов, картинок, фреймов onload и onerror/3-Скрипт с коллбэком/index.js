function addScript(src, callback) {
    let script = document.createElement('script');
    script.src = src;
    let s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(script, s);

    script.onload = callback;
}

addScript('test.js', function() {
    go();
});