function addScript(src) {
    let script = document.createElement('script');
    script.src = src;
    let s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(script, s);
    return script;
}

function addScripts(scripts, callback) {
    let loaded = {};
    let counter = 0;

    for (let i = 0; i < scripts.length; i++)(function(i) {
        let script = addScript(scripts[i]);

        script.onload = function() {
            if (loaded[i]) return;
            loaded[i] = true;
            counter++;
            if (counter === scripts.length) callback();
        };
    }(i));
}


addScripts(["a.js", "b.js", "c.js"], function() {
    a()
});