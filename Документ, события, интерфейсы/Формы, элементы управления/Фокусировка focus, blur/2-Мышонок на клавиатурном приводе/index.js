let mouse = document.getElementById('mouse');

(function () {
    let moveX = 0,
        moveY = 0;

    document.onkeydown = function (e) {
        if (e.keyCode === 37) {
            moveX -= 86;
        } else if (e.keyCode === 38) {
            moveY -= 140;
        } else if (e.keyCode === 39) {
            moveX += 86;
        } else if (e.keyCode === 40) {
            moveY += 140;
        }

        mouse.style.left = moveX + 'px';
        mouse.style.top = moveY + 'px';
    }

})();