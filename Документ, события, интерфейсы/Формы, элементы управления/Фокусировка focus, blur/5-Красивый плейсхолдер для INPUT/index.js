var placeholder = document.getElementById('placeholder');
var input = document.getElementById('input');

placeholder.onclick = function() {
    input.focus();
};

input.onfocus = function() {
    placeholder.parentNode && placeholder.parentNode.removeChild(placeholder);
};