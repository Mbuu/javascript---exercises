let container = document.querySelector('.container');
let closeButtons = document.querySelectorAll('.remove-button');
let blocks = document.querySelectorAll('.pane');

container.onclick = function (event) {
    let target = event.target;

    if (target.tagName === 'BUTTON') {
        target.closest('.pane').style.display = 'none';
    }
};
