let table = document.getElementsByTagName('table')[0];

table.onclick = function (event) {
    if (event.target.tagName !== 'TH') return;

    sortTable(event.target.cellIndex, event.target.getAttribute('data-type'));
};

function sortTable(colNumber, type) {
    let tbody = document.getElementsByTagName('tbody')[0];

    let rowsArray = [].slice.call(tbody.rows);

    let compare;

    switch (type) {
        case 'number':
            compare = function(rowA, rowB) {
                return rowA.cells[colNumber].innerHTML - rowB.cells[colNumber].innerHTML;
            };
            break;
        case 'string':
            compare = function(rowA, rowB) {
                return rowA.cells[colNumber].innerHTML > rowB.cells[colNumber].innerHTML;
            };
            break;
    }

    rowsArray.sort(compare);

    table.removeChild(tbody);

    for (let i = 0; i < rowsArray.length; i++) {
        tbody.appendChild(rowsArray[i]);
    }

    table.appendChild(tbody);
}