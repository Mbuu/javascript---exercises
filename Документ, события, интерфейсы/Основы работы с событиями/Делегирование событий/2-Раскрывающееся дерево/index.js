let tree = document.getElementsByTagName('ul')[0];

let treeItems = document.getElementsByTagName('li');

for (let i = 0; i < treeItems.length; i++) {
     let li = treeItems[i];

     let span = document.createElement('span');
     li.insertBefore(span, li.firstChild);
     span.appendChild(span.nextSibling);
}

tree.onclick = function (event) {
    if (event.target.tagName !== 'SPAN') return;

    if (event.target.nextSibling) {
        event.target.nextSibling.classList.toggle('collapse')
    }
};
