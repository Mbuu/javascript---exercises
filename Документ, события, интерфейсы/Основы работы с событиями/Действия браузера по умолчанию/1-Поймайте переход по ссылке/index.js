contents.addEventListener('click', function (event) {
    let target = event.target;
    if (target.closest('a').nodeName !== 'A') return;
    let href = target.closest('a').getAttribute('href');

    let question = confirm(`Уйти на ${href}`);

    if (!question) {
        event.preventDefault();
    }
});